# **DuckDuckClone**

Your favorite non functional DuckDuckGo clone for vanilla HTML and CSS practice!

Some images are downloaded from duckduckgo and svgs are sourched from sites such as:

- https://www.flaticon.com/authors/those-icons
- https://www.icons8.com
